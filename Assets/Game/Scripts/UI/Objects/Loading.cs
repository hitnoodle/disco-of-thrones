﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour {

	public delegate void Action();
	public event Action OnEndLoading;

	public float TimeLoading;

	private Animator _anim;

	// Use this for initialization
	void Start () {
		_anim			= GetComponent<Animator>();
	}

	public void Activate(){
		StartCoroutine(DoLoading(TimeLoading));
	}

	void GoOut(){
		_anim.SetTrigger("FadeOut");
	}

	IEnumerator DoLoading(float time){
		yield return new WaitForSeconds(time);
		GoOut();
	}

	public void DestroyLoading(){
		if(OnEndLoading != null)
			OnEndLoading();

		this.gameObject.SetActive(false);
	}
}
