﻿using UnityEngine;
using System.Collections;

public class MenuBG : MonoBehaviour {
	private Animator _anim;

	// Use this for initialization
	void Start () {
		_anim		= GetComponent<Animator>();
	}

	public void SlideUp(){
		_anim.SetTrigger("EaseOut");
	}

	public void DestroyMenu(){
		this.gameObject.SetActive(false);
	}
}
