﻿using UnityEngine;
using System.Collections;

public class Counter : MonoBehaviour {
	private SkeletonAnimation _SkelAnim;

	public string _Anim;
	// Use this for initialization
	void Awake () {
		_SkelAnim		= GetComponent<SkeletonAnimation>();
	}

	void Start(){
		PlayAnimation();
	}

	void PlayAnimation(){
		SoundManager.PlaySoundEffect("READY_START.mp3");

		_SkelAnim.state.SetAnimation(0, _Anim, false);
		_SkelAnim.state.End += EndAction;
		
		StopAllCoroutines();
	}

	void EndAction(Spine.AnimationState state, int trackIndex){
		_SkelAnim.state.End -= EndAction;
		StartCoroutine(SetToEnd(0f));
	}
	
	IEnumerator SetToEnd(float wait){
		yield return new WaitForSeconds(wait);
		End();
	}
	
	void End(){
		this.gameObject.SetActive(false);
	}
}
