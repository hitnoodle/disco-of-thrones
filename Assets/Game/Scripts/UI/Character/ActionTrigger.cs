﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionTrigger : MonoBehaviour 
{
	private Animator _anim;
	public int ID		= 0;
	public DiscoPlayer _player;

	private SpriteRenderer _Sprite;

	// Use this for initialization
	void Start () 
    {
//		Debug.Log();
		_anim		= GetComponent<Animator>();
		_Sprite		= GetComponent<SpriteRenderer>();

        _player.OnGoingToBeat   += Prepare;
		_player.OnSwapPlayer	+= Activated;
	}

    void Prepare(int player, double time)
    {
		if(_Sprite != null){
			if (player == ID)
			{
				_Sprite.enabled = true;
				_Sprite.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			}
			else
			{
				_Sprite.enabled = false;
			}
		}
    }

	void Activated(Queue<int> data)
    {
		if(_Sprite != null){
			int	activeData	= data.Peek();
			if(activeData == ID)
			{
				_Sprite.enabled = true;
				_Sprite.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
				
				StartCoroutine(DisableSpriteLater());
			}
			else
			{
				
				_Sprite.enabled	= false;
			}
		}

	}

    IEnumerator DisableSpriteLater()
    {
        yield return new WaitForSeconds(0.4f);

		if(_Sprite != null)
        	_Sprite.enabled = false;
    }
}
