﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallTriggerHandler : MonoBehaviour {

	private PlayerController _Player;
	private MeshRenderer _Renderer;

	private SkeletonAnimation _SkelAnim;

	public float _Time;
	public int ID;

	public DiscoPlayer	_Disco;

	public string IdleAnim;
	public string AttackSuccessAnim;
	public string AttackFailAnim;
	public string AttackSpecialAnim;

	private bool IsShowed;

	private Vector3 _Scale;

	// Use this for initialization
	void Start () {
		_Scale			= transform.localScale;

		_Player			= GetComponentInParent<PlayerController>();
		_Renderer		= GetComponent<MeshRenderer>();
		_SkelAnim		= GetComponent<SkeletonAnimation>();

		_Renderer.enabled		= false;
		_Disco.OnSwapPlayer		+= ShowBall;

		_Player.OnAttack		+= OnAttack;
		_Player.OnSpecial		+= OnSpecial;
		_Player.OnAttackFail	+= OnFail;
	}

	IEnumerator UnSpawn(float time){
		yield return new WaitForSeconds(time);
		End();
	}

	void ShowBall(Queue<int> Data){
		int	activeData	= Data.Peek();

		if(activeData == ID){
			_Renderer.enabled		= true;
			Idle();
			StartCoroutine(UnSpawn(_Time));

			if(_Player.transform.localScale.x < 0){
				transform.localScale	= new Vector3(-_Scale.x, _Scale.y, _Scale.z);
			}
			else{
				transform.localScale	= _Scale;
			}
		}
		else{
//			_Renderer.enabled		= false;
		}
	}

	void Idle(){
		if (_SkelAnim.AnimationName != IdleAnim)
		{
			_SkelAnim.state.SetAnimation(0, IdleAnim, true);
		}
	}

	void OnAttack(){
		_SkelAnim.state.SetAnimation(0, AttackSuccessAnim, false);
		_SkelAnim.state.End += EndAction;

		StopAllCoroutines();
	}

	void OnSpecial(){
		_SkelAnim.state.SetAnimation(0, AttackSpecialAnim, false);
		_SkelAnim.state.End += EndAction;

		StopAllCoroutines();
	}

	void OnFail(){
		_SkelAnim.state.SetAnimation(0, AttackFailAnim, false);
		_SkelAnim.state.End += EndAction;

		StopAllCoroutines();
	}

	void EndAction(Spine.AnimationState state, int trackIndex){
		_SkelAnim.state.End -= EndAction;
		StartCoroutine(SetToEnd(0f));
	}

	IEnumerator SetToEnd(float wait){
		yield return new WaitForSeconds(wait);
		End();
	}

	void End(){
		_Renderer.enabled		= false;
	}
}