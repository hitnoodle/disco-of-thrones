﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

	private Image _Image;

	// Use this for initialization
	void Start () {
		_Image			= GetComponent<Image>();
	}

	public void UpdateBarField(float param, float max){
		if(_Image != null){
			float currentInput		= param/max;
			_Image.fillAmount		= currentInput;
		}
	}
}