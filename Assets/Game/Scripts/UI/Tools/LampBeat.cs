﻿using UnityEngine;
using System.Collections;

public class LampBeat : MonoBehaviour {

	private Light _light;

	public BeatTranslator _Beat;
		
	public float FirstIntensity;
	public float NewIntensity;

	public Color[] _colors;
	private int ParamColor	= 0;

	public float _Time;
	// Use this for initialization
	void Start () {
		_light				= GetComponent<Light>();
		_Beat.OnBeat		+= DoIntensity;	
	}
	
	void DoIntensity(){
		_light.intensity		= NewIntensity;
		_light.color			= _colors[ParamColor];
		ParamColor++;
		if(ParamColor >= _colors.Length)	ParamColor	= 0;
		StartCoroutine(SetToDefault(_Time));
	}

	IEnumerator SetToDefault(float time){
		yield return new WaitForSeconds(time);
		_light.intensity		= FirstIntensity;
		StopAllCoroutines();
	}

	// Update is called once per frame
	void Update () {
	
	}
}
