﻿using UnityEngine;
using System.Collections;

public class CameraEffect : MonoBehaviour {
	private Vector3 originPosition;
	private Quaternion originRotation;
	public float shake_decay;
	public float shake_intensity;

	public PlayerController[] _Players;

	public ItemObject[] _Item;



//	public AttackCollider[] _CharAttackCollider;
//	public Box[] _Boxes;
//	public CharacterCollider _CharCollider;
//
//	public BuildingBlock[] _Buildings;
//	public BuildingParent[] _BuildingParent;

	void Start(){
		
		originPosition = transform.position;

		for(int i=0; i<_Players.Length ;i++){
			_Players[i].OnShake		+= Shake;
		}
		for(int i=0; i<_Item.Length ;i++){
			_Item[i].OnShake		+= Shake;
		}
//		for(int i=0; i<_CharAttackCollider.Length ;i++){
//			_CharAttackCollider[i].OnAttack		+= Shake;
//			_CharAttackCollider[i].OnGetImpact	+= Shake;
//		}
//
//		for(int i=0; i<_Boxes.Length ;i++){
//			_Boxes[i].OnBoxHitted				+= Shake;
//		}
//
//		for(int i=0; i<_Buildings.Length ;i++){
//			_Buildings[i].OnGetImpact		+= Shake;
//		}
//
//		for(int i=0; i<_BuildingParent.Length ;i++){
//			_BuildingParent[i].OnGetImpact	+= Shake;
//		}
	}

	void Update (){
		if (shake_intensity > 0){
			transform.position = originPosition + Random.insideUnitSphere * shake_intensity;
			shake_intensity -= shake_decay;


		}
		else{
			transform.position	= originPosition;
		}
	}
	
	void Shake(float Impact, float Long){
		originPosition = transform.position;
		originRotation = transform.rotation;
		shake_intensity = Impact;
		shake_decay		= Long;
	}
}
