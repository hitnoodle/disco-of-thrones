﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterSequence : MonoBehaviour {
	public DiscoPlayer _Player;

	public SequenceObject[] SeqObject;
	public Vector3[] XObjectPos		= new Vector3[4];
	public int ActiveState;

	public delegate void StateAction(int ActiveAction);
	public event StateAction OnStateChange;

	public delegate void Action();
	public event Action OnStateSwap;

	// Use this for initialization
	void Start () {
		Initialize();
	}

	void Initialize(){
		ActiveState					= -1;
		_Player.OnSwapPlayer		+= ChangeState;

		for(int i=0; i<XObjectPos.Length ;i++){
			XObjectPos[i]	= SeqObject[i].transform.localPosition;
		}
	}

//	void ChangeState(){
//		if(ActiveState >= 0 && ActiveState < SeqObject.Length)
//			SeqObject[ActiveState].Deactivate();
//
//		if(ActiveState + 1 < SeqObject.Length)
//			ActiveState++;
//		else
//			ActiveState	= 0;
//
//		if(ActiveState >= 0 && ActiveState < SeqObject.Length){
//			SeqObject[ActiveState].Activate();
//
//			if(OnStateChange != null)
//				OnStateChange(ActiveState);
//		}
//	}
	
	void ChangeState(Queue<int> Active){
//		if(Active >= 0 && Active < SeqObject.Length){
		int[] Data	= Active.ToArray();

			for(int i=0; i<Data.Length ;i++){
				SeqObject[i].ID	= Data[i];
			}
			

			for(int i=0; i<SeqObject.Length ;i++){
					for(int j=0; j<Data.Length ;j++){
						if(Data[j]	== i+1){
						SeqObject[i].SwapPlace(XObjectPos[j]);
					}
				}	
			}
//		}
	}


}