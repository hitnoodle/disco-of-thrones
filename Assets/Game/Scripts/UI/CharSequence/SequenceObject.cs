﻿using UnityEngine;
using System.Collections;

public class SequenceObject : MonoBehaviour {
	private Transform _Transform;
	private Vector3 _Position;
	private Animator _anim;

	private float TargetPosition;
	public float MoveSpeed;

	public int ID;
	[HideInInspector] public int placeID;

	private bool IsActive		= true;
	// Use this for initialization
	void Awake () {
		_anim			= GetComponent<Animator>();
		_Transform		= transform;
		_Position		= _Transform.localPosition;
		TargetPosition	= _Position.x;
	}

	public void Activate(){
		_anim.SetTrigger("Activate");
	}

	public void Deactivate(){
		_anim.SetTrigger("Deactivate");
	}

	public void SwapPlace(Vector3 Position){
		TargetPosition		= Position.x;
	}

	void Move(){
		_Position		= _Transform.localPosition;

		if(_Position.x < TargetPosition){
			float distance	= MoveSpeed * Time.deltaTime;
			if(_Position.x + distance < TargetPosition)
				_Position.x		+= distance;
			else
				_Position.x		= TargetPosition;
		}	
		else if(_Position.x	> TargetPosition){
			float distance	= MoveSpeed * Time.deltaTime;
			if(_Position.x > TargetPosition)
				_Position.x		-= distance;
			else
				_Position.x		= TargetPosition;
		}

		_Transform.localPosition	= _Position;
	}

	void Update(){
		Move();
	}
}
