﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AI_Pathfinding : MonoBehaviour {

	private GameObject targetObject;
	private Vector3 targetPosition;
	public Seeker seeker;
	public Path path;
	public float speed = 100f;
	public float nextWaypointsDistance = 3.0f;
	private int currentWaypoint = 0;
	public PlayerController controller;
	public PlayerInput playerInput;
	public float timeForUpdatePath = 2f;
	private GameObject[] targetCandidates;

	// Use this for initialization
	void Start () {
		StartCoroutine (GetNewTarget());
		StartCoroutine (AttackHandler());
	}

	IEnumerator GetNewTarget()
	{
		while (true) 
		{
			targetObject = SearchTarget();
			targetPosition = new Vector3(targetObject.transform.position.x, targetObject.transform.position.y, this.transform.position.z);
			GetNewPath ();
			yield return new WaitForSeconds(timeForUpdatePath);
		}
	}

	public GameObject SearchTarget()
	{
		targetCandidates = GameObject.FindGameObjectsWithTag("Player");
		GameObject targetObject = null;
		float closestDistance = float.MaxValue;
		foreach(GameObject targetCandidate in targetCandidates)
		{
			if(!targetCandidate.name.Equals(this.gameObject.name))
			{
				float tempDistance = Vector3.Distance(this.transform.position, targetCandidate.transform.position);
				if(tempDistance < closestDistance)
				{
					closestDistance = tempDistance;
					targetObject = targetCandidate;
				}
			}
		}

		if (targetObject == null)
			return this.gameObject;
		else
			return targetObject;
	}

	void GetNewPath()
	{
		seeker.StartPath (transform.position, targetPosition, OnPathComplete);
	}

	void OnPathComplete(Path newPath)
	{
		if (!newPath.error) 
		{
			path = newPath;
			currentWaypoint = 0;
		}
	}

	IEnumerator AttackHandler()
	{
		while (true) {
			if (targetObject != null && Vector3.Distance (transform.position, targetObject.transform.position) < 1f &&
				playerInput._discoPlayer.CanPlayerAttack (playerInput.Player)) {
				controller.Attack ();
				yield return new WaitForSeconds (1f);
			}
			yield return null;
		}
	}

	void FixedUpdate()
	{
		if (path == null) 
		{
			return;
		}

		if (currentWaypoint >= path.vectorPath.Count) 
		{
			return;
		}


		Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized * speed * Time.fixedDeltaTime;
		controller.Move (dir.x , dir.y);

		Vector3 currentPathWayPoint = new Vector3 (path.vectorPath [currentWaypoint].x, path.vectorPath [currentWaypoint].y, transform.position.z);
		if (Vector3.Distance (transform.position, currentPathWayPoint) < nextWaypointsDistance) {
			currentWaypoint++;
		}
	}
}
