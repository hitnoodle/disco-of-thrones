﻿using UnityEngine;
using System.Collections;

public class GridScanner : MonoBehaviour {
	public float scanDelay = 10f;

	// Use this for initialization
	void Start () {
		StartCoroutine (scanOnNTime(scanDelay));
	}
	
	IEnumerator scanOnNTime(float time){
		while (true) {
			yield return new WaitForSeconds(time);
			AstarPath.active.Scan();
		}
	}
}
