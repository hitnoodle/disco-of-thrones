﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatsSceneController : MonoBehaviour 
{
    public AudioClip Song;
    public AudioSource Player;

    public SpriteRenderer _Sprite;
    public BeatTranslator _Translator;
    public DiscoPlayer _DiscoPlayer;

	// Use this for initialization
	void Start () 
	{
        _Translator.OnBeat += BeatSprite;

        _DiscoPlayer.OnSwapPlayer += ShowPlayerSequence;
        _DiscoPlayer.StartPlayer(2.0F);
	}

    void ShowPlayerSequence(Queue<int> sequence)
    {
        int[] seq = sequence.ToArray();
        Debug.Log(seq[0] + " " + seq[1] + " " + seq[2] + " " + seq[3]);
    }

    void BeatSprite()
    {
        StartCoroutine(BeatSpriteRoutine());
    }

    IEnumerator BeatSpriteRoutine()
    {
        _Sprite.transform.localScale = new Vector3(15, 15, 15);

        yield return new WaitForSeconds(0.05f);

        _Sprite.transform.localScale = new Vector3(10, 10, 10);
    }
	// Update is called once per frame
	void Update () 
	{
        if (Input.GetKeyDown(KeyCode.Space)) Debug.Log(_DiscoPlayer.CanPlayerAttack(4));
        else if (Input.GetKeyDown(KeyCode.S)) _DiscoPlayer.ShufflePlayers();
	}
}
