﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DiscoPlayer : MonoBehaviour 
{
    public delegate void _OnGoingToBeat(int player, double time);
    public event _OnGoingToBeat OnGoingToBeat;

    public delegate void _OnSwapPlayer(Queue<int> sequence);
    public event _OnSwapPlayer OnSwapPlayer;

    private BeatTranslator _BeatTranslator;

    private List<int> _Players;
    private Queue<int> _PlayerSequence;

	public List<int> Players{
		get{
			return _Players;
		}
	}

	// Use this for initialization
	void Awake () 
	{
        _BeatTranslator = GetComponent<BeatTranslator>();

        _Players = new List<int>();
        _Players.Add(1);
        _Players.Add(2);
        _Players.Add(3);
        _Players.Add(4);

        ShufflePlayers();

        _BeatTranslator.OnPreparingBeat += PreparePlayerBeat;
        _BeatTranslator.OnBeat += SwapPlayer;
	}

    private void PreparePlayerBeat(double preparationTime)
    {
        int[] sequenceArray = _PlayerSequence.ToArray();
        if (OnGoingToBeat != null && sequenceArray.Length > 1)
        {
            OnGoingToBeat(sequenceArray[1], preparationTime);
        }
    }

    private void SwapPlayer()
    {
		if (_PlayerSequence.Count <= 0) return;

        int oldPlayer = _PlayerSequence.Dequeue();
        _PlayerSequence.Enqueue(oldPlayer);

        /*
        int[] sequenceArray = _PlayerSequence.ToArray();
        string log = "";
        for (int i = 0; i < sequenceArray.Length; i++)
        {
            log += sequenceArray[i] + " ";
        }
        Debug.Log(log);
        */

        if (OnSwapPlayer != null) OnSwapPlayer(_PlayerSequence);
    }

    // Not consistent, player here = Disco Player where as player in other function = game player
    public void StartPlayer(double delay)
    {
        _BeatTranslator.StartBeats(delay);
    }

    #region Game Player Interactions

    public void ShufflePlayers()
    {
        _Players = _Players.OrderBy(s => System.Guid.NewGuid()).ToList<int>();

        _PlayerSequence = new Queue<int>();
        foreach (int i in _Players)
            _PlayerSequence.Enqueue(i);
    }

    public bool CanPlayerAttack(int player)
    {
        return _BeatTranslator.IsNearBeat() && _PlayerSequence.Peek() == player;
    }

    public void KillPlayer(int id)
    {
        if (_Players.Contains(id))
        {
            _Players.Remove(id);

            List<int> queue = _PlayerSequence.ToList<int>();
            queue.Remove(id);

            _PlayerSequence = new Queue<int>();
            foreach (int i in queue)
                _PlayerSequence.Enqueue(i);

        }
    }

    #endregion
}
