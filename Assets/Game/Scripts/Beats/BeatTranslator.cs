﻿using UnityEngine;
using System.Collections;

public class BeatTranslator : MonoBehaviour
{
    public AudioClip Song;
    public AudioSource Player;

    public float BPM = 122.0f;
    public double BeatPreparationDelay = 0.5F;

    public double[] BPMChangeTimes = new double[3];
    public float[] BPMs = new float[3];

    [System.Serializable]
    public class TimeRange
    {
        public double Start;
        public double End;
    }
    public TimeRange[] IgnoreBeatRanges;

    // Range on Ignore Beat Ranges
    public int[] SpeedUpIndex;

    public delegate void _OnPreparingBeat(double preparationTime);
    public event _OnPreparingBeat OnPreparingBeat;

    public delegate void _OnBeat();
    public event _OnBeat OnBeat;

    public delegate void _OnSpeedUp();
    public event _OnSpeedUp OnSpeedUp;

    private double _NextEventTime;
    private double _BeatTime;

    private int _Flip = 0;
    private bool _Running = false;

    private double _StartTime;
    private int _TimesIndex = 0;

    private int _IgnoreIndex = 0;
    private bool _SpeedEventTriggered = false;

    private IEnumerator _BeatEnumerator;

    // Use this for initialization
    void Start()
    {
        BPM = BPMs[_TimesIndex];
        CalculateBeatTime();
    }

    public void StartBeats(double delay)
    {
        _NextEventTime = AudioSettings.dspTime + delay;
        _StartTime = _NextEventTime;

        Player.clip = Song;
        Player.PlayScheduled(_StartTime);

        _Running = true;
    }

    void CalculateBeatTime()
    {
        _BeatTime = 60.0F / (double)BPM;
    }

    // TODO: TEST THIS SHIT
    bool IgnoreThisRange(double time)
    {
        if (IgnoreBeatRanges.Length > 0 && _IgnoreIndex < IgnoreBeatRanges.Length)
        {
            if (time > _StartTime + IgnoreBeatRanges[_IgnoreIndex].Start)
            {
                if (time <= _StartTime + IgnoreBeatRanges[_IgnoreIndex].End)
                {
                    // Check whether we need to display speed up
                    if (!_SpeedEventTriggered)
                    {
                        foreach (int i in SpeedUpIndex)
                        {
                            if (i == _IgnoreIndex)
                            {
                                if (OnSpeedUp != null) OnSpeedUp();
                                //Debug.Log(i);

                                _SpeedEventTriggered = true;
                                break;
                            }
                        }

                        // Check only first time
                        _SpeedEventTriggered = true;
                    }

                    return true;
                }
                else
                {
                    _IgnoreIndex++;
                    _SpeedEventTriggered = false;

                    return false;
                }
            }
        }

        return false;
    }

    // Update is called once per frame
    void Update()
    {
        // Already started?
        if (!_Running) return;

        // Need to ignore current range?
        double time = AudioSettings.dspTime;
        if (IgnoreThisRange(time)) return;

        // Need to change BPM?
        if (time > _StartTime + BPMChangeTimes[_TimesIndex])
        {
            // Change it
            if (_TimesIndex < BPMChangeTimes.Length - 1)
            {
                _TimesIndex++;

                BPM = BPMs[_TimesIndex];
                CalculateBeatTime();

                //_NextEventTime = _StartTime + BPMChangeTimes[_TimesIndex - 1] + _BeatTime;
                //Beat(_NextEventTime, BeatPreparationDelay);

                _NextEventTime += _BeatTime;
                _Flip = 1 - _Flip;
            }
            else // Loop the last part
            {
                Player.time = (float)BPMChangeTimes[_TimesIndex - 1];
                _StartTime = time - Player.time;
                _NextEventTime += _BeatTime;
            }
        }
        else if (time + BeatPreparationDelay > _NextEventTime) // Beat it... BEAT IT
        {
            Beat(_NextEventTime, BeatPreparationDelay);

            _NextEventTime += _BeatTime;
            _Flip = 1 - _Flip;
        }
    }

    #region Beat

    void Beat(double time, double preparation)
    {
        // Reset if already on
        if (_BeatEnumerator != null) ResetBeat();

        // Wait and call delegate when beat
        double delay = _NextEventTime - AudioSettings.dspTime;
        _BeatEnumerator = WaitAndBeat(delay);
        StartCoroutine(_BeatEnumerator);
    }

    void ResetBeat()
    {
        StopCoroutine(_BeatEnumerator);
        _BeatEnumerator = null;
    }

    IEnumerator WaitAndBeat(double time)
    {
        //Debug.Log("Before: " + AudioSettings.dspTime + " " + time);

        if (OnPreparingBeat != null && time > 0) OnPreparingBeat(time);

        yield return new WaitForSeconds((float)time);

        //Debug.Log("Beat: " + AudioSettings.dspTime + " " + time);

        if (OnBeat != null)
            OnBeat();
    }

    public bool IsNearBeat()
    {
        return Mathf.Abs((float)_NextEventTime - (float)AudioSettings.dspTime) < 1f;
    }

    #endregion
}
