﻿using UnityEngine;
using System.Collections;

public class ItemObject : MonoBehaviour {
	private Transform _Transform;
	private Vector3 _Position;

	public DiscoPlayer _Disco;

	public delegate void Action(float param, float _long);
	public event Action OnShake;

	// Use this for initialization
	void Awake () {
		_Transform		= transform;
		_Position		= _Transform.localPosition;
	}

	void Start(){
		SoundManager.PlaySoundEffect("AYAM");
	}

	public void Inititalize(Vector3 _Pos){
		_Position						= _Pos;

//		_Position.z						= _Transform.localPosition.z;
		_Transform.localPosition		= _Position;
	}

	public void UnSpawn(){
		this.gameObject.SetActive(false);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			SoundManager.PlaySoundEffect("AYAM");

			_Disco.ShufflePlayers();
			UnSpawn();

			if(OnShake != null)
				OnShake(0.7f,0.05f);
		}
	}
}
