﻿using UnityEngine;
using System.Collections;

public class ItemSpawner : MonoBehaviour {

	public BeatTranslator _Beat;

	public ItemObject[] _Items;

	public float MinX;
	public float MaxX;
	public float MinY;
	public float MaxY;
	
	public int ParamSpawn;

	// Use this for initialization
	void Start () {
		_Beat.OnBeat		+= SpawnItem;
	}

	void SpawnItem(){
		ParamSpawn++;

		if(ParamSpawn % 20 == 0){
			int rand		= Random.Range(0,_Items.Length);

			for(int i=0; i<= rand ; i++){
				float posX	= Random.Range(MinX,MaxX);
				float posY	= Random.Range(MinY, MaxY);
				
				if(!_Items[i].gameObject.activeInHierarchy){
					_Items[i].gameObject.SetActive(true);
					_Items[i].Inititalize(new Vector3(posX,posY,posY));
				}
				
			}
		}

	}
}
