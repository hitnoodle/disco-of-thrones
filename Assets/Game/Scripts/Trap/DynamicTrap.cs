﻿using UnityEngine;
using System.Collections;

public class DynamicTrap : Trap {
	public Vector2 startPosition, endPosition;
	public bool looping;
	public float delay;
	public float speed;

	private bool atStart;
	private bool unSpawn;

	public delegate void _OnFinish();
	public _OnFinish OnFinish;

	void Start()
	{
		this.transform.localPosition = new Vector3 (this.transform.localPosition.x ,this.transform.localPosition.y ,this.transform.localPosition.y);
		OnActive(true);
		atStart = true;
		unSpawn = false;
	}

	public void SetActiveTrap(bool active)
	{
		trapCollider.enabled = active;
		if (active)
			StartCoroutine (TrapBehavior ());


	}

	public IEnumerator TrapBehavior()
	{
		SoundManager.PlaySoundEffect("MILEY");
		Vector3 tempEnd;
		Vector3 tempStart;
		do
		{
			yield return new WaitForSeconds(delay);
			float timeElapsed = 0;

			if (atStart) {
				tempStart = startPosition;
				tempEnd = endPosition;
			} else {
				tempStart = endPosition;
				tempEnd = startPosition;
			}

			tempEnd.Set(tempEnd.x, tempEnd.y, tempEnd.y);

			while (true) 
			{
				yield return null;
				transform.localPosition = Vector3.Lerp(tempStart, tempEnd, timeElapsed);
				if(Vector3.Distance (transform.localPosition, tempEnd) < 1f) 
				{
					if(currentTime >= lifeTime)
					{
						OnUnspawn(false);
					}
					atStart = !atStart;
					break;
				}
				timeElapsed += Time.deltaTime * speed;
			}
		}
		while(looping && !unSpawn);
	}
}