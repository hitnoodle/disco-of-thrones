﻿using UnityEngine;
using System.Collections;
using Pathfinding;
public class TrapAnimation : MonoBehaviour {

    SkeletonAnimation _animation;
   
	public string ActiveAnimation = "";
    public string SpawnAnimation = "";
    public string IdleAnimation = "";
    public string UnspawnAnimation = "";

    Trap trap;

	// Use this for initialization
	void OnEnable () {
        trap = GetComponent<Trap>();
        _animation = GetComponent<SkeletonAnimation>();

        trap.OnSpawn += PlaySpawnAnimation;
        trap.OnUnspawn += PlayUnspawnAnimation;
        trap.OnIdle += PlayIdleAnimation;
		trap.OnActive += PlayActiveAnimation;
	}

    void Start()
    {
        
    }

    void OnDisable()
    {
		trap.OnSpawn -= PlaySpawnAnimation;
		trap.OnUnspawn -= PlayUnspawnAnimation;
		trap.OnIdle -= PlayIdleAnimation;
		trap.OnActive -= PlayActiveAnimation;
	}
	
	void PlayIdleAnimation(bool repeat)
    {
		if (_animation.AnimationName != IdleAnimation )
        {
            _animation.state.SetAnimation(0, IdleAnimation, repeat);
			_animation.state.Complete += CheckIdleAnimation;
        }
    }

	void PlaySpawnAnimation(bool repeat)
	{
		if (_animation.AnimationName != SpawnAnimation)
		{
			_animation.state.SetAnimation(0, SpawnAnimation, repeat);
			_animation.state.End += CheckSpawnAnimation;
		}
	}

	void PlayUnspawnAnimation(bool repeat)
	{
		trap.OnSpawn -= PlaySpawnAnimation;
		trap.OnUnspawn -= PlayUnspawnAnimation;
		trap.OnIdle -= PlayIdleAnimation;
		trap.OnActive -= PlayActiveAnimation;
		_animation.state.End -= CheckActiveAnimation;
		_animation.state.End -= CheckSpawnAnimation;

		if (UnspawnAnimation == "") {
			gameObject.layer = 0;
			DestroyObject (transform.parent.gameObject);
		}

		if (_animation.AnimationName != UnspawnAnimation && UnspawnAnimation != "")
		{
			_animation.state.SetAnimation(0, UnspawnAnimation, repeat);
			_animation.state.End += CheckUnspawnAnimation;
		}

	}

	void PlayActiveAnimation(bool repeat)
	{
		if (_animation.AnimationName != ActiveAnimation)
		{
			_animation.state.SetAnimation(0, ActiveAnimation, repeat);
			_animation.state.End += CheckActiveAnimation;
		}
	}

	void CheckIdleAnimation(Spine.AnimationState state, int trackIndex, int temp)
	{
		if(trap.currentTime >= trap.lifeTime){
			_animation.state.Complete -= CheckIdleAnimation;
			StartCoroutine (SwitchOnUnspawn());
		}
	}

	void CheckSpawnAnimation(Spine.AnimationState state, int trackIndex)
	{
		_animation.state.End -= CheckSpawnAnimation;
		StartCoroutine (SwitchOnIdle());
	}

	void CheckActiveAnimation(Spine.AnimationState state, int trackIndex)
	{
		_animation.state.End -= CheckActiveAnimation;
		trap.FinishActive ();
	}

	void CheckUnspawnAnimation(Spine.AnimationState state, int trackIndex)
	{
		_animation.state.End -= CheckUnspawnAnimation;
		DestroyObject (transform.parent.gameObject);
	}

	IEnumerator SwitchOnIdle()
	{
		yield return null; 
		trap.OnIdle (true);
	}

	IEnumerator SwitchOnUnspawn()
	{
		yield return null; 
		PlayUnspawnAnimation (false);
	}
}
