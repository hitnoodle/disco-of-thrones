﻿using UnityEngine;
using System.Collections;

public class DynamicTrapController : MonoBehaviour {
	public bool needActivated = false;
	public DynamicTrap trap;
	public BoxCollider2D activatorCollider;
	private bool unSpawn;

	// Use this for initialization
	void Start () 
	{
		transform.localPosition = trap.transform.localPosition;
		if (!needActivated) {
			trap.SetActiveTrap (true);
		}
	}

	public void OnMouseDown() {
		if (needActivated && !unSpawn) {
			activatorCollider.enabled = false;
			trap.SetActiveTrap (true);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player" && needActivated && !unSpawn) {
			activatorCollider.enabled = false;
			trap.SetActiveTrap (true);
		}
	}

}
