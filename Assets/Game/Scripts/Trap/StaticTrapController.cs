﻿using UnityEngine;
using System.Collections;

public class StaticTrapController : MonoBehaviour {

	public bool needActivated = false;
	public StaticTrap trap;
	public BoxCollider2D activatorCollider;

	// Use this for initialization
	void Start () 
	{
		transform.localPosition = trap.transform.localPosition;
		if (!needActivated) {
			trap.ActiveTrap ();
		} 
		else {
			trap.OnActiveEnd += ActiveEnd;
		}
	}

	public void OnMouseDown() {
		if (needActivated) {
			activatorCollider.enabled = false;
			trap.ActiveTrap ();
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player" && needActivated)
		{
			activatorCollider.enabled = false;
			trap.ActiveTrap ();
		}
	}

	void ActiveEnd()
	{
		if(activatorCollider != null) activatorCollider.enabled = true;
	}
}
