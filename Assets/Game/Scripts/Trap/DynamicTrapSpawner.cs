﻿using UnityEngine;
using System.Collections;

public class DynamicTrapSpawner : MonoBehaviour {
	public float min_Y,max_Y;
	public float minDelay,maxDelay;
	
	private static string miley_path = "Trap Miley";
	public float damage = 30f;
	public float damage_iterator;
	public float speed = 0.1f;
	public float speed_iterator;

	private GameObject trapInstance;
	// Use this for initialization
	void Start () 
	{
		StartCoroutine (spawn());
	}
	
	public IEnumerator spawn()
	{
		while (true) {
			if(trapInstance == null){
			yield return new WaitForSeconds (Random.Range(minDelay,maxDelay));
			GameObject trap_gameobject = Resources.Load<GameObject> (miley_path);
				if (trap_gameobject != null) {
					//Ubah parameter trap yang selalu bertambah
					GameObject trap = trap_gameobject.transform.FindChild("TrapObject").gameObject;
					float newx;
					if(Random.Range(0,2) == 0) newx = 20f;
					else newx = -20f;
					float newy = Random.Range (min_Y, max_Y);
					trap.transform.localPosition = new Vector3 (newx, newy, newy);
					
					DynamicTrap dT = trap.GetComponent<DynamicTrap>();
					if(dT != null){
						dT.speed = speed;
						dT.Damage = damage;
						dT.startPosition = new Vector2(newx, newy);
						dT.endPosition = new Vector2(-newx, newy);
					}

					//Instansiasi ke hierarchy
					trapInstance = Instantiate (trap_gameobject) as GameObject;
					Vector3 temp_position = trapInstance.transform.localPosition;
					trapInstance.transform.parent = this.transform;
					trapInstance.transform.localPosition = temp_position;
					
				}

			damage += damage_iterator;
			speed += speed_iterator;
			}
			else{
				yield return null;
			}
		}
	}
}
