﻿using UnityEngine;
using System.Collections;

public class StaticTrapSpawner : MonoBehaviour {
	public float min_X,min_Y,max_X,max_Y;
	public float delay;

	private static string ikan_path = "Trap Ikan";
	private int amount;
	private int damage;
	public int damage_iterator;
	public float minLifeTime,maxLifeTime;

	// Use this for initialization
	void Start () {
		amount = 1;
		damage = 50;
		StartCoroutine (spawn());
	}
	
	public IEnumerator spawn()
	{
		while (true) {
			yield return new WaitForSeconds (delay);
			for (int i =0; i < amount; i++) {
				GameObject trap_gameobject = Resources.Load<GameObject> (ikan_path);
				if (trap_gameobject != null) {

					GameObject trap = trap_gameobject.transform.FindChild("TrapObject").gameObject;
					float newy = Random.Range (min_Y, max_Y);
					trap.transform.localPosition = new Vector3 (Random.Range (min_X, max_X), newy, newy);

					StaticTrap sT = trap.GetComponent<StaticTrap>();
					if(sT != null){
						sT.Damage = damage;
						sT.lifeTime = Random.Range(minLifeTime,maxLifeTime);
					}

					GameObject instance = Instantiate (trap_gameobject) as GameObject;

					Vector3 temp_position = instance.transform.localPosition;
					instance.transform.parent = this.transform;
					instance.transform.localPosition = temp_position;
			
				}
			}
			amount++;
			damage += damage_iterator;
		}
	}
}
