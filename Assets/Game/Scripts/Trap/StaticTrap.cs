﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class StaticTrap : Trap {
	void Start()
	{
		this.transform.localPosition = new Vector3 (this.transform.localPosition.x ,this.transform.localPosition.y ,this.transform.localPosition.y);
		OnSpawn(false);
		SetActiveTrap (false);
	}
	
	public void ActiveTrap()
	{
		SetActiveTrap (true);
		if(OnActive != null)
		OnActive(false);


		SoundManager.PlaySoundEffect("IKAN");
	}

	public IEnumerator TrapLifeTime()
	{
		yield return new WaitForSeconds(lifeTime);

		if(OnUnspawn != null)
		OnUnspawn (false);
	}


}
