﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {
	public enum TrapType
	{
		statics,
		dynamics
	}

	public TrapType type;

	public delegate void _OnIdle(bool repeat);
	public _OnIdle OnIdle;

	public delegate void _OnSpawn(bool repeat);
	public _OnSpawn OnSpawn;

	public delegate void _OnUnspawn(bool repeat);
	public _OnUnspawn OnUnspawn;

	public delegate void _OnActive(bool repeat);
	public _OnUnspawn OnActive;

	public delegate void _OnActiveEnd();
	public _OnActiveEnd OnActiveEnd;

	public BoxCollider2D trapCollider;

	public float Damage;
	
	private bool byActivated;

	public float lifeTime;
	[HideInInspector]
	public float currentTime;
	
	public void SetActiveTrap(bool active)
	{
		trapCollider.enabled = active;
	}

	void Update()
	{
		currentTime += Time.deltaTime;
	}

	public void FinishActive(){
		if (type == TrapType.statics) {
			StartCoroutine(SwitchOnIdle());
			SetActiveTrap (false);
			OnActiveEnd ();
		}
	}

	IEnumerator SwitchOnIdle()
	{
		yield return null;
		if(currentTime < lifeTime)
			OnIdle (true);
	}
}
