﻿using UnityEngine;
using System.Collections;

public class WinScene : MonoBehaviour {
	

	// Use this for initialization
	void OnEnable () {
		StartCoroutine(ReloadScene(8f));
	}

	IEnumerator ReloadScene(float time){
		yield return new WaitForSeconds(time);
		Application.LoadLevel("MainScene");
	}

}
