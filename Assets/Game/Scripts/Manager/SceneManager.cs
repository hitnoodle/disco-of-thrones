﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {
	public GameObject SpeedUp;
	public GameObject Counter;

	public DiscoPlayer _Player;

//	public bool StateIsPlayed			= false;
//
	public enum GameState{
		MENU_SCENE,
		LOADING_SCENE,
		GAME_SCENE
	}

	public GameState _CurrentState;

	public BeatTranslator _Beat;

	public MenuBG MainMenu;
	public Loading _Loading;
	public GameObject _Object;
	public GameObject _CanvasObj;
	public CC_AnalogTV _analog;
	public GameObject Players;

	// Use this for initialization
	void Start () {
		SoundManager.PlayBackgroundMusic("MAIN MENU LOOP",true);

		MenuStart();
		_Beat.OnSpeedUp			+= ActivateSpeedUp;
		_Loading.OnEndLoading	+= GameStart;
	}

	void MenuStart(){
		_CurrentState		= GameState.MENU_SCENE;
	}

	void Update(){
		if(_CurrentState == GameState.MENU_SCENE){
			if(Input.anyKeyDown){
				MainMenu.SlideUp();
				_CurrentState	= GameState.LOADING_SCENE;
				_Loading.Activate();
				_Object.SetActive(true);
				
				_analog.enabled			= false;
			}
		}
	}

	void GameStart(){
		SoundManager.FadeBackgroundMusicOut(1f);

		_CanvasObj.SetActive(true);
		_CurrentState	= GameState.GAME_SCENE;

		Players.SetActive(true);

		StartCoroutine(SetGameActive(2f));
	}

	IEnumerator SetGameActive(float time){
		yield return new WaitForSeconds(time);
		_Player.StartPlayer(1F);
		Counter.SetActive(true);
	}

	void ActivateSpeedUp(){
		SpeedUp.SetActive(true);
	}

//
//	void GameStart(){
//		ChangeState(GameState.CUT_SCENE);
//	}
//
//	void ChangeState(GameState _State){
//		if(_State != _CurrentState){
//			_CurrentState	= _State;
//			StateIsPlayed	= true;
//		}
//	}
//
//	void EndState(){
//		StateIsPlayed		= false;
//	}
//
//	// Update is called once per frame
//	void Update () {
//		if(StateIsPlayed)	return;
//	}
}