﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

    public int Player;
    PlayerController _controller;
	public DiscoPlayer _discoPlayer;
	public bool isAI = false;

	// Use this for initialization
    void Awake()
    {
        _controller = GetComponent<PlayerController>();
        _controller.PlayerID = Player;
    }
	
	// Update is called once per frame
    void FixedUpdate()
    {
		if (!isAI) {
			if (Input.GetButtonDown ("Joystick" + Player + "Attack")) {
				if (_discoPlayer.CanPlayerAttack (Player))
					_controller.Attack ();
				else
					_controller.TakeDamage (0f);
			}
			if (Input.GetButtonDown ("Joystick" + Player + "Special")) {
				_controller.SpecialAttack ();
			}

			float h = 0;
			float v = 0;

			if (Player == 1 || Player == 2) {
				h = Input.GetAxis ("Keyboard" + Player + "Horizontal");
				v = Input.GetAxis ("Keyboard" + Player + "Vertical");
			}

			if (Input.GetJoystickNames ().Length == 1 && Player == 1) {
				h = Input.GetAxis ("Joystick1Horizontal");
				v = Input.GetAxis ("Joystick1Vertical");
			} else if (Input.GetJoystickNames ().Length == 2) {
				if (Player == 1) {
					h = Input.GetAxis ("Joystick1Horizontal");
					v = Input.GetAxis ("Joystick1Vertical");
				} else if (Player == 2) {
					h = Input.GetAxis ("Joystick2Horizontal");
					v = Input.GetAxis ("Joystick2Vertical");
				}
			} else if (Input.GetJoystickNames ().Length > 2) {
				h = Input.GetAxis ("Joystick" + Player + "Horizontal");
				v = Input.GetAxis ("Joystick" + Player + "Vertical");
			}

			_controller.Move (h, v);
		}
    }
}
