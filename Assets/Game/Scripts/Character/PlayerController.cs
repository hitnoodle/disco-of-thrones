﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    Character character;

	public GameObject WinScene;

	public DiscoPlayer _discoPlayer;

    public Collider2D SpecialAttackCollider;
    public Collider2D AttackCollider;
    public Xft.XffectComponent SpecialFX;
    public Xft.XffectComponent DeathFX;
	public Xft.XffectComponent AttackFX;

    public float Speed = 0.2f;
    public float friction = 0.2f;
    public float CurrentAngle = 0f;
    public bool Moveable = true;
    Rigidbody2D _rigidbody;
    Vector2 velocity = new Vector2(0, 0);

    public delegate void _OnAttack();
    public event _OnAttack OnAttack;
	public event _OnAttack OnAttackFail;

    public delegate void _OnIdle();
    public event _OnIdle OnIdle;

    public delegate void _OnRunning();
    public event _OnRunning OnRunning;

    public delegate void _OnSpecial();
    public event _OnSpecial OnSpecial;

	public delegate void _OnDamage(float damage);
	public event _OnDamage OnDamage;

	public delegate void OnShakeDamaged(float impact, float _long);
	public event OnShakeDamaged OnShake;

	private float scale;

    [HideInInspector]
    public string soundName;

    [HideInInspector]
    public int PlayerID;

	void Awake () {
        character = GetComponent<Character>();
        _rigidbody = GetComponent<Rigidbody2D>();
        SpecialAttackCollider.enabled = false;
        AttackCollider.enabled = false;

		scale		= transform.localScale.x;
	}

    void Start()
    {
        if (PlayerID == 1)
            soundName = "AFRO";
        else if (PlayerID == 2)
            soundName = "MADONA";
        else if (PlayerID == 3)
            soundName = "MJ";
        else soundName = "BEAR";
    }
	
	void Update () 
    {
        if (CurrentAngle < 0)
        {
			if(transform.localScale.x > 0)
            	gameObject.transform.localScale = new Vector3(-scale, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        }
        else if(CurrentAngle > 0)
        {
            gameObject.transform.localScale = new Vector3(scale, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        }
	}

    public void Trapped()
    {

    }

    public void TakeDamage(float damage, bool knockedRight)
    {
        if (OnDamage != null)
        {
            SoundManager.PlaySoundEffectOneShot(soundName + "_HIT DAMAGE");
            OnDamage(damage);
            Knockback(knockedRight);

			if(OnShake != null)
				OnShake(0.5f,0.05f);
        }
	}

    //wrong beat
    public void TakeDamage(float damage)
    {
        if (OnDamage != null)
        {
            SoundManager.PlaySoundEffectOneShot(soundName + "_HIT DAMAGE");
            OnDamage(damage);
            character.ResetCombo();

			if(OnAttackFail != null)
				OnAttackFail();
        }
    }

    public void Attack()
    {
        if (OnAttack != null)
        {
            if (character.CurrentCombo < character.MaxCombo)
            {
                if (OnAttack != null)
                {
                    OnAttack();
                    SoundManager.PlaySoundEffectOneShot(soundName + "_ATTACK");
					AttackFX.Active();
                }
            }
            else
            {
                if (OnSpecial != null)
                {
                    OnSpecial();
                    SoundManager.PlaySoundEffectOneShot(soundName + "_SPECIAL");
                    SpecialFX.Active();
                }
            }
        }
    }

    public void SpecialAttack()
    {
        if (OnSpecial != null)
        {
            OnSpecial();
        }
    }

    void Knockback(bool knockedRight)
    {
        float force;
        if(knockedRight)
            force = 2000f;
        else
            force = -2000f;

        _rigidbody.AddForce(new Vector3(force, transform.position.y));
    }

    public void Move(float h, float v)
    {
		if (!Moveable)
        {
            h = 0f;
            v = 0f;
        }
        else
        {
            if (h != 0f || v != 0f)
            {
                if (OnRunning != null) OnRunning();
                CurrentAngle = Mathf.Atan2(h, v) * Mathf.Rad2Deg;
            }
            else if (h == 0f && v == 0f)
            {
                if (OnIdle != null) OnIdle();
            }
        }

        velocity.x += h * Speed;
        velocity.y += v * Speed;


        _rigidbody.velocity = velocity;
        velocity *= friction;

		gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, gameObject.transform.localPosition.y); 
    }

    void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "Attack")
        {
            Character enemy = coll.gameObject.GetComponentInParent<Character>();
            //float angle = Mathf.Atan2(coll.transform.parent.localPosition.x - transform.position.x, coll.transform.parent.localPosition.y - transform.position.y) * Mathf.Rad2Deg;

            var angle = Vector3.Angle(Vector3.up, coll.transform.parent.localPosition - transform.localPosition);

            bool knockedRight;
            if (coll.transform.parent.localPosition.x < transform.localPosition.x)
                knockedRight = true;
            else
                knockedRight = false;
            
            TakeDamage(enemy.Damage, knockedRight);
		} else if(coll.tag == "Trap") {
			Trap trap = coll.gameObject.GetComponentInParent<Trap>();
			TakeDamage(trap.Damage);
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		if(coll.collider.tag == "Trap") {
			Trap trap = coll.gameObject.GetComponentInParent<Trap>();
			
			TakeDamage(trap.Damage);
		}
	}

    public void PlayerDied()
    {
		_discoPlayer.KillPlayer(PlayerID);

		if(_discoPlayer.Players.Count == 1){
			WinScene.SetActive(true);
//			StartCoroutine(ReloadScene(2f));
		}

		Destroy(this.gameObject);
    }

//	IEnumerator ReloadScene(float time){
//		yield return new WaitForSeconds(time);
//		ResetScene();
//	}

//	void ResetScene(){
//		Application.LoadLevel("MainScene");
//	}
}
