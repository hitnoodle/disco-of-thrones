﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {

    Character character;
    SkeletonAnimation _animation;
    bool Interruptable = false;

    public string IdleAnimation;
    public string RunAnimation;
    public string AttackAnimationRight;
    public string AttackAnimationLeft;
    public string SpecialAnimation;
	public string DamageAnimation;
	public string DeathAnimation;
    private Animator _colliderAnim;

    PlayerController _controller;

	// Use this for initialization
	void OnEnable () {        
        _controller.OnAttack += PlayAttackAnimation;
        _controller.OnRunning += PlayRunningAnimation;
        _controller.OnIdle += PlayIdleAnimation;
        _controller.OnSpecial += PlaySpecialAnimation;
		_controller.OnDamage += PlayDamageAnimation;
	}

    void Awake()
    {
        _controller = GetComponent<PlayerController>();
        _animation = GetComponent<SkeletonAnimation>();
        character = GetComponent<Character>();
    }

    void OnDisable()
    {
        _controller.OnAttack -= PlayAttackAnimation;
        _controller.OnRunning -= PlayRunningAnimation;
		_controller.OnIdle -= PlayIdleAnimation; 
		_controller.OnSpecial -= PlaySpecialAnimation;
		_controller.OnDamage -= PlayDamageAnimation; 
    }

    void PlayAttackAnimation()
    {
        if (_animation.AnimationName != AttackAnimationRight && !Interruptable)
        {
            _controller.AttackCollider.enabled = true;
            Interruptable = true;
            _controller.Moveable = false;
            _animation.state.SetAnimation(0, AttackAnimationRight, false);
			_animation.state.End += CheckAnimation;
        }
    }

    void PlayRunningAnimation()
    {
        if (_animation.AnimationName != RunAnimation && !Interruptable)
        {
            _animation.state.SetAnimation(0, RunAnimation, true);
        }
    }

    void PlayIdleAnimation()
    {
        if (_animation.AnimationName != IdleAnimation && !Interruptable)
        {
            _animation.state.SetAnimation(0, IdleAnimation, true);
        }
    }

    void PlaySpecialAnimation()
    {
        if (_animation.AnimationName != SpecialAnimation && !Interruptable)
        {
            _controller.SpecialAttackCollider.enabled = true;
            Interruptable = true;
            _controller.Moveable = false;
            _animation.state.SetAnimation(0, SpecialAnimation, false);
			_animation.state.End += CheckAnimation;
        }
    }

	void PlayDamageAnimation(float damage) {
		if (_animation.AnimationName != DamageAnimation && !Interruptable)
		{
			Interruptable = true;
			_controller.Moveable = false;
			character.TakeDamage(damage);
            if (character.Health <= 0)
            {
                _controller.DeathFX.Active();
                _animation.state.SetAnimation(0, DeathAnimation, false);
                Time.timeScale = 0.2f;
                SoundManager.PlaySoundEffectOneShot(_controller.soundName + "_HIT DAMAGE");
                 _animation.state.End += PlayerDies;
            }
            else
            {
                _animation.state.SetAnimation(0, DamageAnimation, false);
                _animation.state.End += CheckAnimation;
            }

		}
	}

	void CheckAnimation(Spine.AnimationState state, int trackIndex)
    {
        _controller.SpecialAttackCollider.enabled = false;
        _controller.AttackCollider.enabled = false;
        _animation.state.End -= CheckAnimation;
        Interruptable = false;
        _controller.Moveable = true;

		if(_animation.AnimationName != DamageAnimation)
        	character.AddCombo();
    }

    void PlayerDies(Spine.AnimationState state, int trackIndex)
    {
        Time.timeScale = 1f;

        _animation.state.End -= PlayerDies;
        _controller.PlayerDied();
    }

}
