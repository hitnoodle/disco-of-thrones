﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    public float Health = 1000;
    public float Damage;
    public int CurrentCombo;
    public int MaxCombo;

	public ProgressBar _HealthBar;
	private float MaxHealth;

	// Use this for initialization
	void Start () {
		MaxHealth		= Health;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TakeDamage(float damage)
    {
        Health -= damage;
		_HealthBar.UpdateBarField(Health, MaxHealth);
    }

    public void AddCombo()
    {
        if (CurrentCombo < MaxCombo)
            CurrentCombo++;
        else
            CurrentCombo = 0;
    }

	public void ResetCombo() {
		CurrentCombo = 0;
	}
}
