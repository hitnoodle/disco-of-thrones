
party-bear.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
party-bear/party-body
  rotate: true
  xy: 344, 11
  size: 229, 262
  orig: 229, 262
  offset: 0, 0
  index: -1
party-bear/party-hair
  rotate: false
  xy: 2, 2
  size: 130, 74
  orig: 130, 74
  offset: 0, 0
  index: -1
party-bear/party-hand
  rotate: true
  xy: 2, 78
  size: 162, 340
  orig: 162, 340
  offset: 0, 0
  index: -1
party-bear/party-head
  rotate: true
  xy: 855, 36
  size: 204, 116
  orig: 204, 116
  offset: 0, 0
  index: -1
party-bear/party-leg
  rotate: false
  xy: 608, 44
  size: 245, 196
  orig: 245, 196
  offset: 0, 0
  index: -1
